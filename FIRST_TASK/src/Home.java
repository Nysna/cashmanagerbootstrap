import java.io.*;

public class Home {
    public static void main(String[] args) {
        User.login("Maïssa", "Nouhaud");
        System.out.println(User.isLogged);
        User.viewSettings();
        Admin.changeSettings("Maïssa", true);
        User.viewSettings();
    }
}
