import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class User {
    public static boolean isLogged = false;

    public static void login(String name, String password) {
        isLogged = true;
    }

    public static void viewSettings() {
        try {
            File myFile = new File("Settings.txt");
            Scanner myReader = new Scanner(myFile);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
