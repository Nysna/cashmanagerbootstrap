import java.io.*;

public class Admin extends User {
    public static void changeSettings(String field, boolean Value) {
            FileWriter fileWriter = null;
            try {
                fileWriter = new FileWriter("Settings.txt", true);
                fileWriter.write("\n"+field+":"+Value);
        } catch (Exception e) {
                e.printStackTrace();
        } finally {
                try {
                    if (fileWriter != null) {
                        fileWriter.flush();
                        fileWriter.close();
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }
}
