package model;

public class Account {
    private String Name;
    private String Cash;

    public Account(){
        this.Name = "";
        this.Cash = "";
    }
    public void setName(String name){
        this.Name = name;
    }
    public void setCash(String cash) {
        this.Cash = cash;
    }
    public String getName() {
        return this.Name;
    }
    public String getCash() {
        return this.Cash;
    }
}