package model;
import model.Account;

import java.util.ArrayList;

public class User {
    private String name;
    private String[] accountNumber;
    private ArrayList<Account> accounts;

    public User() {
        this.name="";
    }

    public void setName(String name){this.name = name; }
    public void setAccountNumber(String[] accountNumber){this.accountNumber = accountNumber; }
    public void setAccounts(ArrayList<Account> accounts) {this.accounts = accounts;}

    public String getName() {return this.name;}
    public String[] getAccountNumber() {return this.accountNumber;}
    public ArrayList<Account> getAccounts() {return this.accounts;}
}
