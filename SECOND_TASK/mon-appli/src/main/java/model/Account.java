package model;

public class Account {
    private String cash;

    public Account() {
        this.cash = "";
    }
    public void setCash(String cash) {
        this.cash = cash;
    }
    public String getCash() {
        return this.cash;
    }
}