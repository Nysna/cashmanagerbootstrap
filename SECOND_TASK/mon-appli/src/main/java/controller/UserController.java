package controller;
import model.User;

import java.util.Arrays;
import java.util.Scanner;

public class UserController {
    private User model;

    public UserController(User model) {this.model = model;}
    public void readTerminal() {
        String name = "";
        String accountNumber = "";
        String[] userAccounts = null;
        Scanner myObj = new Scanner(System.in);

        System.out.println("What is your name?");
        name = myObj.nextLine();
        System.out.println("What are your account numbers?");
        accountNumber = myObj.nextLine();
        userAccounts = accountNumber.split(",");
        model.setAccountNumber(userAccounts);
    }
}
