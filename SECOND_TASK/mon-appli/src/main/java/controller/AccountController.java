package controller;
import model.Account;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class AccountController {
    private ArrayList<Account> accounts = new ArrayList<Account>();

    public AccountController(ArrayList<Account> account) {
        this.accounts = account;
    }

    public void readJsonFile(String fileName, String[] userAccounts) {
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(fileName))
        {
            Object jsonData = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) jsonData;

            //GET LIGNE PAR LIGNE LE "ACCOUNT NUMBER " -> IF ACCOUNT NUMBER EST DANS USERACCOUNTS :
            String cash = (String) jsonObject.get("cash");
            /*
    POUR REMPLIR LE ACCOUNT TABLEAU AVEC LES VALEURS DE CASH :
            for(String cash:cashData) {
                Account account = new Account();
                accounts.setCash(cash);
                this.accounts.add(account);
            }
*/
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}