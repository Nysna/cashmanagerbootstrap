import model.Account;
import model.User;
import controller.AccountController;
import controller.UserController;

public class App
{
    public static void main(String[] args )
    {
        UserController userController = new UserController(new User());
        userController.readTerminal();
    }
}